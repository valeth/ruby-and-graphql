# frozen_string_literal: true

require "graphql"
require_relative "data"

module Mutations
  class CreateInventoryItem < GraphQL::Schema::Mutation
    argument :name, String, required: true
    argument :value, Integer, required: true

    field :success, Boolean, null: false
    field :errors, [String], null: false

    def resolve(name:, value:)
      id = rand(1_000_000_000...9_999_999_999)
      item = InventoryItem.new(id, name, value)
      INVENTORY << item

      # No errors for our pseudo "database"
      { success: true, errors: [] }
    end
  end

  class RemoveInventoryItem < GraphQL::Schema::Mutation
    argument :name, String, required: false
    argument :id, ID, required: false

    field :success, Boolean, null: false
    field :errors, [String], null: false

    def resolve(**args)
      if args[:id] || args[:name]
        INVENTORY.delete_if { |e| e.id == args[:id].to_i || e.name == args[:name] }
        { success: true, errors: [] }
      else
        { success: false, errors: ["Valid name or ID required"] }
      end
    end
  end
end

module Types
  class InventoryItem < GraphQL::Schema::Object
    field :id, ID, null: false
    field :name , String, null: false
    field :value, Integer, null: false
  end

  class Query < GraphQL::Schema::Object
    field :items, [InventoryItem], null: false

    def items
      INVENTORY
    end
  end

  class Mutation < GraphQL::Schema::Object
    field :createInventoryItem, mutation: ::Mutations::CreateInventoryItem
    field :removeInventoryItem, mutation: ::Mutations::RemoveInventoryItem
  end
end

class Schema < GraphQL::Schema
  query Types::Query
  mutation Types::Mutation
end
