# frozen_string_literal: true

require "sinatra"
require "sinatra/json"
require "rack/contrib"

require_relative "data"
require_relative "schema"


class InventoryApp < Sinatra::Base
  # to parse JSON payload as params
  use Rack::PostBodyContentTypeParser

  get "/items" do
    json INVENTORY
  end

  post "/graphql" do
    result =
      if params[:query]
        Schema.execute(params[:query], variables: params[:variables], operation_name: params[:operationName])
      elsif request.content_type == "application/graphql"
        Schema.execute(request.body.read)
      else
        { errors: ["Invalid content type"] }
      end

    json result
  end
end
