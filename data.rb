# frozen_string_literal: true

InventoryItem = Struct.new(:id, :name, :value)

# Our pseudo database
INVENTORY = [
  InventoryItem.new(1249826395, "Hammer", 12),
  InventoryItem.new(4124873432, "Bow", 23),
  InventoryItem.new(5937412312, "Sword", 100),
  InventoryItem.new(2747312123, "Potion", 240)
]
